var express = require('express');
var fs = require('fs');
var request = require('request');
var cheerio = require('cheerio');
var CronJob = require('cron').CronJob;
var app     = express();

app.get('/scrape', function(req, res){
    var job = new CronJob('*/20 * * * * *', function() {
        var toDo = JSON.parse(fs.readFileSync('./rutas.1.json', 'utf8'));
        if(toDo.length==0){
            fs.unlinkSync("./rutas.1.json")
            job.stop()
            var temp = JSON.parse(fs.readFileSync('./results/jobs.json', 'utf8'));
            request({method: 'POST',uri:"https://hackatontsdl.mybluemix.net/offers", body:temp, json : true}, function(error, response, body) {
                if(error){
                    console.log("Error "+error)
                } else if(response.statusCode != 200){
                    console.log("Something happen: "+response.statusCode)
                } else if(response.statusCode == 200){
                    console.log("The job offers have been loaded")
                }

            })
        } else {
            var newArray = toDo.slice(1,toDo.length)
            fs.writeFileSync("./rutas.1.json", JSON.stringify(newArray, null, 4));
            var originalTime = (new Date()).getTime();
            console.log("INFO: retrieving "+ toDo[0].path+" - 1 try, on time: "+originalTime)
            getJobsList(toDo[0]);
        }
    }, null, true, 'America/Los_Angeles');

    function getJobsList(pageToVisit, tryy){
        request(pageToVisit.path, function(error, response, body) {
            if(error && tryy < 11){
                var petitionTime = (new Date()).getTime()
                while((petitionTime-originalTime)<1000){
                    petitionTime = (new Date()).getTime();
                }
                getJobsList(pageToVisit, tryy+1)
            }
            else if(response.statusCode !== 200) {
                // the url content was unable to get after 10 tries
                console.log("FAIL: retrieving "+pageToVisit.path+" - page retrieving error. Status Code: "+response.statusCode)
                // The url is added to unretrieved urls file 
                var obj = JSON.parse(fs.readFileSync('./missing.json', "utf8"));
                obj[obj.length]={"element":pageToVisit.path,"statusCode":response.statusCode}
                fs.writeFileSync('./missing.json',JSON.stringify(obj,null,4),"utf8")
                console.log("INFO: New toRetrieve task has been added: "+ JSON.stringify(obj[obj.length-1]))
             }
             else if(response.statusCode === 200) {
                 // Parse the document body
                 console.log("SUCCESS: retrieved "+pageToVisit.path+" on try: "+tryy)
                 var $ = cheerio.load(body);
                 var jobs = $('.info')
                 var links = []
                 for(var i = 0; i < jobs.length; i++){
                    links[i]={"category":pageToVisit.category,"url":"https://clasificados.eltiempo.com"+jobs[i].children[0].children[0].attribs.href,"title":jobs[i].children[0].children[0].attribs.title}
                 }
                 // process link list retrieved from url
                 processJobList(links);
             } else {
                 // the url content was unable to get after 10 tries
                 console.log("FAIL: retrieving "+pageToVisit.path+" - after 10 tries the content was unable to get")
                 // The url is added to unretrieved urls file 
                 var obj = JSON.parse(fs.readFileSync('./faltantes.json', "utf8"));
                 obj[obj.length]={"element":parent}
                 fs.writeFileSync('./faltantes.json',JSON.stringify(obj,null,4),"utf8")
                 console.log("INFO: New toRetrieve task has been added: "+ JSON.stringify(obj[obj.length-1]))
            }
        })
    }

    function processJobList(list){
        if (!fs.existsSync('./results/jobs.json')) {
            fs.writeFileSync('./results/jobs.json', JSON.stringify([]));
        }
        for(var j = 0; j<list.length; j++){
            getJobOffer(list[j])
        }
    }

    function getJobOffer(offer){
        request(offer.url, function(error, response, body) {
            if(response.statusCode === 200) {
                // Parse the document body
                console.log("SUCCESS: retrieved "+offer.url)
                var $ = cheerio.load(body);
                var description = $(".descripcion")
                var cmpDsc = description[0].children[0].data
                var tmpDsc = description[0].children[0].data
                var email = ""
                if(cmpDsc.includes("@")){
                    var pos = tmpDsc.indexOf("@")
                    encontrado = false
                    for(i = pos; i>-1 && !encontrado;i--){
                        if(tmpDsc.charAt(i)==" " || tmpDsc.charAt(i)==","){
                            tmpDsc = tmpDsc.substr(i+1,tmpDsc.length)
                            encontrado = true;
                        }
                    }
                    found = false
                    for(j = tmpDsc.indexOf("@"); j<tmpDsc.length && !found;j++){
                        if(tmpDsc.charAt(j)==" " || tmpDsc.charAt(j)==","){
                            tmpDsc = tmpDsc.substr(0,j)
                            found = true
                        }
                    }
                    email = tmpDsc
                }
                var extraInfo = $("div.campo")
                var city = extraInfo[0].children[1].data
                var salary = "";
                if(extraInfo[1]!=undefined && extraInfo[1].children.length>1){
                    salary = extraInfo[1].children[1].data
                }
                offer.description = cmpDsc
                offer.email = email
                offer.city = city
                offer.salary = salary
                var riskProb = Math.random()
                var risk = (riskProb<0.15)?"Confiable":(riskProb<0.35)?"Leve":(riskProb<0.60)?"Moderado":"Alto";
                offer.risk = risk
                var offertas = JSON.parse(fs.readFileSync('./results/jobs.json', 'utf8'))
                offertas.push(offer)
                fs.writeFileSync("./results/jobs.json", JSON.stringify(offertas, null, 4));
            }
        })
    }
})

app.listen('8080')
console.log('Magic happens on port 8081');
exports = module.exports = app;